package com.training.lol.controller;

import com.training.lol.model.LolChampion;
import com.training.lol.service.LolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/api/v1/lolChampion")
public class LolController {
    @Autowired
    private LolService lolService;

    @GetMapping
    public List<LolChampion> findAll(){
        return lolService.findAll();
    }

    @GetMapping("{id}")
    public ResponseEntity<LolChampion> findById(@PathVariable String id) {
        try {
            return new ResponseEntity<LolChampion>(lolService.findById(id).get(), HttpStatus.OK);
        } catch (NoSuchElementException ex) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping
    public LolChampion save(@RequestBody LolChampion lolChampion){
        return lolService.save(lolChampion);
    }

    @DeleteMapping("{id}")
    public ResponseEntity delete(@PathVariable String id) {
        if(lolService.findById(id).isEmpty()) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        lolService.delete(id);

        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
