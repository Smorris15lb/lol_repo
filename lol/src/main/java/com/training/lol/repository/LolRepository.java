package com.training.lol.repository;

import com.training.lol.model.LolChampion;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;

@Component
public interface LolRepository extends MongoRepository<LolChampion, String> {
}
