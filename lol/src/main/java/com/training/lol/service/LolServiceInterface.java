package com.training.lol.service;

import com.training.lol.model.LolChampion;

import java.util.List;
import java.util.Optional;

public interface LolServiceInterface {

    List<LolChampion> findAll();

    LolChampion save(LolChampion pokemon);

    Optional<LolChampion> findById(String id);
    void delete(String id);
}
