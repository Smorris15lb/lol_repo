package com.training.lol.service;

import com.training.lol.model.LolChampion;
import com.training.lol.repository.LolRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;
import java.util.Optional;

@Component
public class LolService implements LolServiceInterface{
    @Autowired
    private LolRepository lolRepository;
    public List<LolChampion> findAll(){
        return lolRepository.findAll();
    }

    public Optional<LolChampion> findById(String id) { return lolRepository.findById(id); }

    public LolChampion save(LolChampion lolChampion){
        return lolRepository.save(lolChampion);
    }

    public void delete(String id) {
        lolRepository.deleteById(id);
    }


}
