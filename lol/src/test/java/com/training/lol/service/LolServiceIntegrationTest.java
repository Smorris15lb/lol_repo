package com.training.lol.service;

import com.training.lol.model.LolChampion;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class LolServiceIntegrationTest {

    @Autowired
    private LolService lolService;


    @Test
    public void testPokemonServiceFindAll() {
        List<LolChampion> allLol = lolService.findAll();

        assertEquals(3, allLol.size());
        System.out.println(allLol.get(0));
    }
}
