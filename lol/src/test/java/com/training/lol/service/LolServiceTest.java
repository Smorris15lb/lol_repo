package com.training.lol.service;

import com.training.lol.model.LolChampion;
import com.training.lol.repository.LolRepository;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@SpringBootTest
public class LolServiceTest {

    @Autowired
    private LolService lolService;

    @MockBean
    private LolRepository lolRepository;

    @Test
    public void testPokemonServiceFindAll() {
        List<LolChampion> allLol = new ArrayList<LolChampion>();
        LolChampion testLol = new LolChampion();
        testLol.setId("adbcd");
        allLol.add(testLol);

        when(lolRepository.findAll()).thenReturn(allLol);

        assertEquals(1, lolService.findAll().size());
    }
}
