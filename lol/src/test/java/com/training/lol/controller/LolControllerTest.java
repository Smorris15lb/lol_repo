package com.training.lol.controller;


import com.training.lol.model.LolChampion;
import com.training.lol.service.LolServiceInterface;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(LolController.class)
public class LolControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private LolServiceInterface lolService;

    @Test
    public void testLolControllerFindAll() throws Exception{
        String testId = "abcd";
        List<LolChampion> allLolChampion = new ArrayList<LolChampion>();
        LolChampion testLol = new LolChampion();
        testLol.setId("abcd");
        allLolChampion.add(testLol);

        when(lolService.findAll()).thenReturn(allLolChampion);
        StringBuilder builder = new StringBuilder("\"id\":\"\"");
        builder.insert(6, testId);
        System.out.println(builder.toString());
        //String match = "\"id\":\"abcd\"";
        this.mockMvc.perform(get("/api/v1/lolChampion"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.content().string(containsString(builder.toString())));
    }
}
